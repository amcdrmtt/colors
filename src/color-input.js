import { h, Component } from "preact"

export default class ColorInput extends Component {
  componentDidUpdate() {
    this.input.focus()
  }

  update = e => this.props.setColor(e.target.value)

  accept = e => {
    if (e.key === "Tab" || e.key === "Enter") {
      e.preventDefault()
      this.props.suggestion && this.props.setColor(this.props.suggestion)
    }
  }

  swap = e => {
    e.preventDefault()
    this.props.equivalent && this.props.setColor(this.props.equivalent)
  }

  render({ color, suggestion, equivalent, textColor }) {
    return (
      <div>
        <div class="container">
          <span style={{ color: textColor }}>
            {suggestion || color}
          </span>
          <input
            type="text"
            ref={input => (this.input = input)}
            value={color}
            onInput={this.update}
            onKeyDown={this.accept}
            autofocus
            style={{ color: textColor }}
          />
        </div>
        <a href="#" onClick={this.swap} style={{ color: textColor }}>
          {equivalent}
        </a>
        <style jsx>{`
          .container {
            position: relative;
            margin: 47vh 0 0 10vw;
            width: 80vh;
            height: 6vw;
          }

          a {
            display: inline-block;
            left: 10vw;
            margin-top: 1vh;
            padding: 0;
            font-weight: bold;
            font-size: 2vh;
            opacity: 0.5;
            text-decoration: none;
            position: absolute;
            z-index: 3;
          }

          input,
          span {
            font-family: Menlo, Monaco, Arial;
            text-align: left;
            display: inline-block;
            font-weight: bold;
            font-size: 6vw;
            background-color: transparent;
            text-transform: uppercase;
            border: none;
            padding: 0;
            margin: 0;
            width: 90vw;
            position: absolute;
            opacity: 0.5;
          }

          span {
            z-index: 1;
          }

          input {
            z-index: 2;
          }

          input:focus,
          input:active {
            outline: none;
          }
        `}</style>
      </div>
    )
  }
}
