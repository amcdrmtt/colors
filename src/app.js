import { h, Component } from "preact"
import {
  getRandomColor,
  getTextColor,
  getSuggestion,
  getEquivalent
} from "./colors"
import InfoBox from "./info-box"
import ColorInput from "./color-input"

export default class App extends Component {
  componentWillMount() {
    this.setColor(
      window.location.hash ? window.location.hash.substr(1) : getRandomColor()
    )
  }

  randomize = e => {
    e.preventDefault()
    this.setColor(getRandomColor())
  }

  setColor = color => {
    if (color.startsWith("#") && color.length > 7) color = color.substr(0, 7)

    window.location.hash = color.startsWith("#") ? `#${color}` : color
    if (this.root) this.root.style.backgroundColor = color

    const suggestion = getSuggestion(color)
    const equivalent = getEquivalent(color) || getEquivalent(suggestion)
    this.setState({
      color,
      suggestion,
      equivalent
    })
  }

  render(props, { color, suggestion, equivalent }) {
    const bkgd = (this.root && this.root.style.backgroundColor) || color
    const textColor = getTextColor(bkgd)

    return (
      <div
        ref={div => (this.root = div)}
        class="root"
        style={{ background: color }}
        onDblClick={this.randomize}
      >
        <ColorInput
          color={color}
          suggestion={suggestion}
          equivalent={equivalent}
          textColor={textColor}
          setColor={this.setColor}
        />
        <InfoBox />
        <style jsx>{`
          .root {
            position: fixed;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            font-family: Menlo, Monaco, Arial;
            transition: background-color 0.5s ease;
            overflow: hidden;
          }
        `}</style>
      </div>
    )
  }
}
