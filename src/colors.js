import namedColors from "./named-colors"

export function getTextColor(bkgdColor) {
  let r, g, b
  if (bkgdColor.startsWith("rgb")) {
    const rgb = bkgdColor
      .replace(/rgba?\(/, "")
      .replace(")", "")
      .split(",")
      .map(val => val.trim())
    r = parseInt(rgb[0], 10)
    g = parseInt(rgb[1], 10)
    b = parseInt(rgb[2], 10)
  } else {
    bkgdColor = bkgdColor.toUpperCase()
    const namedColor = namedColors.find(
      c => c.name.toUpperCase() === bkgdColor || c.hex === bkgdColor
    )
    let hex
    if (!namedColor) {
      if (bkgdColor.startsWith("#") && bkgdColor.length === 4) {
        hex = `#${bkgdColor[1]}${bkgdColor[1]}${bkgdColor[2]}${bkgdColor[2]}${bkgdColor[3]}${bkgdColor[3]}`
      } else if (bkgdColor.startsWith("#") && bkgdColor.length === 7) {
        hex = bkgdColor
      }
    } else {
      hex = namedColor.hex
    }

    if (!hex) return "black"

    r = parseInt(hex.substr(1, 2), 16)
    g = parseInt(hex.substr(3, 2), 16)
    b = parseInt(hex.substr(5, 2), 16)
  }

  const yiq = (r * 299 + g * 587 + b * 114) / 1000
  return yiq >= 128 ? "black" : "white"
}

export const getRandomColor = () =>
  namedColors[Math.floor(Math.random() * namedColors.length)].name

export const getEquivalent = value => {
  const color = getColor(value)
  return color
    ? isValidHex(value)
      ? color.name
      : isValidNamedColor(value) ? color.hex : null
    : null
}

export const getSuggestion = value => {
  if (!value) return null
  value = value.toUpperCase()
  return isValidHex(value)
    ? suggestHex(value)
    : isValidNamedColor(value) ? suggestName(value) : null
}

const isValidHex = value => value.startsWith("#") && value.length > 1
const isValidNamedColor = value => !value.startsWith("#") && value.length > 0

const getColor = value =>
  value
    ? namedColors.find(
        c =>
          c.hex.toUpperCase() === value.toUpperCase() ||
          c.name.toUpperCase() === value.toUpperCase()
      )
    : null

const suggestHex = value => {
  const namedColor = namedColors.find(c =>
    c.hex.toUpperCase().startsWith(value)
  )
  return (namedColor && namedColor.hex) || null
}

const suggestName = value => {
  const names = namedColors
    .filter(
      c =>
        c.name.toUpperCase().startsWith(value) && c.name.toUpperCase() !== value
    )
    .sort((c1, c2) => c1.name.length - c2.name.length)
  return (names && names.length && names[0].name) || null
}
