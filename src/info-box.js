import { h, Component } from "preact"

export default class InfoBox extends Component {
  state = { minimized: true }

  componentWillMount() {
    if (localStorage)
      this.setState({ minimized: localStorage.getItem("minimized") === "true" })
    else this.setState({ minimized: true })
  }

  update(minimized) {
    if (localStorage) localStorage.setItem("minimized", minimized)
    this.setState({ minimized })
  }

  show = e => {
    e.preventDefault()
    this.state.minimized && this.update(false)
  }

  hide = e => {
    e.preventDefault()
    e.stopPropagation()
    this.update(true)
  }

  render(props, { minimized }) {
    return (
      <div class={`container ${minimized ? "minimized" : "maximized"}`}>
        <button class="show" onClick={minimized ? this.show : this.hide}>
          {minimized ? "Info" : "Close"}
        </button>

        <h2>Hello!</h2>
        <p>Type the name of a color or its hex value.</p>
        <p>Double-click the background for a random color.</p>
        <p>Hit Enter or Tab to select suggestion.</p>
        <p>
          <a
            href="https://gitlab.com/amcd/colors"
            class="external"
            rel="nofollow noopener"
          >
            Gitlab
          </a>
          <a
            href="https://twitter.com/amcdrmtt"
            class="external"
            rel="nofollow noopener"
          >
            Twitter
          </a>
        </p>
        <style jsx>{`
          .container {
            max-width: calc(100% - 6em);
            background-color: white;
            position: fixed;
            bottom: 0;
            top: auto;
            right: 1em;
            padding: 2em;
            box-shadow: -4px -4px 4px rgba(0, 0, 0, 0.2);
            font-size: 0.8em;
            z-index: 10;
          }

          .container.minimized {
            bottom: auto;
            top: 100%;
            box-shadow: none;
          }

          h2 {
            padding: 0;
            margin: 0;
          }

          .external {
            text-decoration: none;
            display: inline-block;
            margin: 1em 2em 0 0;
            color: rgba(0, 0, 0, 0.5);
          }

          .show {
            border: none;
            background: white;
            color; rgba(0, 0, 0, 0.5);
            display: block;
            font-size: 14px;
            height: 3em;
            position: absolute;
            top: -3em;
            right: 0;
            cursor: pointer;
            font-weight: bold;
            display: flex;
            padding: 0 2em;
            align-items: center;
            box-shadow: -4px -4px 4px rgba(0, 0, 0, 0.2);
            outline: none;
          }

          .hide {
            font-weight: bold;
            font-size: 1em;
            float: right;
            color: black;
            margin-top: 1em;
          }
        `}</style>
      </div>
    )
  }
}
