const path = require("path")

module.exports = {
  entry: path.resolve(__dirname, "./index.js"),
  output: {
    path: path.resolve(__dirname, "./public"),
    filename: "app.js"
  },
  resolve: {
    alias: {
      react: "preact"
    }
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          {
            loader: "babel-loader",
            query: {
              presets: [["es2015", { modules: false }]],
              plugins: [
                "styled-jsx/babel",
                ["transform-react-jsx", { pragma: "h" }],
                "transform-class-properties"
              ]
            }
          }
        ]
      }
    ]
  }
}
